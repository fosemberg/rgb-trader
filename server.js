let ACTION = {
  BAR: 'bar',
  BAR_HISTORY: 'bar-history',
  DICTIONARY: 'dictionary',
  AUTH: 'auth'
}

let TYPE = {
  SUBSCRIBE: 'SUBSCRIPTION',
  UNSUBSCRIBE: 'UNSUBSCRIPTION',
  REQUEST: 'REQUEST',
  EVENT: 'EVENT',
  RESPONSE: 'RESPONSE',
}

// id одого отправленного message
let aid = 1;
// id подписки
let sid = 1;
// rid - id запроса на клиенте

let RESPONSE = {
  CONNECTED: {
    connected: true
  },
  NOT_CONNECTED: {
    connected: false
  },
  DICTIONARY: {
    type: TYPE.RESPONSE,
    action: ACTION.DICTIONARY,
    body: {
      data: "some data"
    },
    rid: 3,
  },
  AUTH: {
    type: TYPE.RESPONSE,
    action: TYPE.AUTH,
    body: {
      account_id: 2226522,
      currency_id: 840,
      session_id: 15694985,
    },
    rid: 1
  }
}

let interval = {
  bar: 0
}

function refreshInterval(innerInterval, func, time) {
  clearInterval(innerInterval);
  innerInterval = setInterval(func, time);
  return innerInterval;
}

let http = require('http');
let express = require('express');
let WSS = require('ws').Server;

let app = express().use(express.static('public'));
let server = http.createServer(app);
server.listen(8082, '127.0.0.1');

let wss = new WSS({port: 8083});

sendMessage = (json) => {
  wss.clients.forEach(function each(client) {
    let outJson = JSON.stringify(json);
    client.send(outJson);
    console.log('Send: ' + outJson);
  });
}

barsHistory = [];

wss.on('connection', function (socket) {
  console.log('Opened Connection 🎉');

  let json = JSON.stringify(RESPONSE.CONNECTED);
  socket.send(json);
  console.log('Sent: ' + json);

  // обработка приходящих сообщений
  // здесь нужно осущесвить подписки
  socket.on('message', function (message) {
    // {"type":"REQUEST","rid":3,"action":"dictionary","body":{}}
    console.log('Received: ' + message);
    let data = JSON.parse(message);
    let action = data.action;
    let type = data.type;

    switch (type) {
      case TYPE.SUBSCRIBE:
        switch (action) {
          case ACTION.BAR:
            sendMessage(generateBarHistory());
            interval.bar = refreshInterval(interval.bar, () => barSend(barsHistory), 1000);
            break;
          default:
            sendMessage({message: 'unknown'})
        }
        break;
      case TYPE.UNSUBSCRIBE:
        switch (action) {
          case ACTION.bar:
            clearInterval(interval.bar);
        }

        break;
      case TYPE.REQUEST:
        switch (action) {
          case ACTION.AUTH:
            sendMessage(RESPONSE.AUTH);
            break;
          case ACTION.DICTIONARY:
            sendMessage(RESPONSE.DICTIONARY);
            break;
          default:
            sendMessage({message: 'unknown'})
        }
        break;
      default:
        sendMessage({message: 'unknown'});
    }
  });

  socket.on('close', function () {
    console.log('Closed Connection');
    clearInterval(interval.bar);
    clearInterval(interval.rate);
  });

});

let rand = () => Math.round(Math.random() * 255 + 1);

let generateBarData = () => ({
  r: rand(),
  g: rand(),
  b: rand(),
})

let generateBarBody = () => ({
  bar: generateBarData(),
  ts: new Date().getTime(),
})

let generateBarHistoryBody = () => {
  let body = []
  for (let i = 0; i < 30; i++) {
    body.push(generateBarBody())
  }
  return body;
}

let generateBarHistory = () => ({
  type: TYPE.RESPONSE,
  action: ACTION.BAR_HISTORY,
  body: generateBarHistoryBody(),
  rid: 1,
  sid: sid++
})

let generateBarJson = () => ({
  type: TYPE.EVENT,
  action: "bar",
  aid: aid++,
  sid: sid,
  body: generateBarBody(),
})

let barSend = function (store) {
  let json = generateBarJson();
  if (store) {
    store.push(json);
    writeToDb(store);
  }
  sendMessage(json);
};

let writeToDb = (json, fileName = 'db/main.json') => {
  let fs = require("fs");
  fs.writeFile(fileName, JSON.stringify(json), "utf8", () => console.log(`writeToDb: ${json}`));
};