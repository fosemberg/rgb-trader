import * as React from 'react';
import {BarView} from "./barView/BarView";
import {CrxClient} from "./CrxClient";
import Color from "./color/container";
// import Color from "./color/Color";

export class App extends React.Component<any> {
  private crxClient: CrxClient = new CrxClient();

  public render(): JSX.Element {
    return (
      <div>
        <BarView crxClient = {this.crxClient}/>
        <Color/>
      </div>
    );
  }
}

