import * as React from 'react';
import StarRating from './components/starRating/StarRating'
import { FaTrash } from 'react-icons/fa'
import './Color.scss'

export interface Props {
  title: string,
  color: string,
  rating?: number,
  onRemove?: () => any,
  onRate?: (x: number) => void,
  className?: string,
}

const Color = ({
  title,
  color,
  rating = 0,
  onRemove = (x: any) => x,
  onRate = (x: number) => x,
  className = '',
}: Props) =>
  <section
    className={className + " color"}
  >
    <h1>{title}</h1>
    <button
      onClick={onRemove}
    >
      <FaTrash />
    </button>
    <div
      className="color"
      style={{ backgroundColor: color }}
    >
    </div>
    <div>
      <StarRating
        starsSelected={rating}
        onRate={onRate}
      />
    </div>
  </section>

export default Color
