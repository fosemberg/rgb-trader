import * as React from 'react';
import ColorView from './Color';

interface Props {
  isOffline?: boolean;
}

interface State {
  title: string,
  color: string,
  rating: number,
  isHide: boolean,
}

class Color extends React.Component <Props, State> {
  public static defaultProps: Partial<Props> = {
    isOffline: true,
  }

  private timeBeforeNewApearing = 250;

  constructor(props: Props) {
    super(props);
    this.state = this.getNewData();
  }

  randomColor = () => '#' + Math.floor(Math.random() * 16777215).toString(16)

  randomString = () => Math.random().toString(36).substring(2, 6)

  getNewData = () => (
    this.props.isOffline
      ? this.generateRandomState()
      : this.getDataFromServer()
  )

  generateRandomState = () => ({
    title: this.randomString(),
    color: this.randomColor(),
    rating: 0,
    isHide: false,
  })

  getDataFromServer = () => {
    // TODO: realize getting data from server
    return this.generateRandomState()
  }

  setRandomState = () => {
    this.setState(this.getNewData())
  }

  onRate = (rating: number) => {
    this.setState({
      rating,
      isHide: true,
    });
    setTimeout(
      () => {
        this.setRandomState();
      },
      this.timeBeforeNewApearing);
  }

  render() {
    return (
      <ColorView
        className={this.state.isHide && "hide"}
        {...this.state}
        onRate={this.onRate}
        onRemove={this.setRandomState}
      />
    )
  }
}

export default Color;