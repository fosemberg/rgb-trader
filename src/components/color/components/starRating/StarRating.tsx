import * as React from 'react';
import Star from './components/star/Star'

interface Props {
  starsSelected: number,
  totalStars?: number,
  onRate: (x: number) => void,
}

const StarRating = ({
                      starsSelected = 0,
                      totalStars = 5,
                      onRate = (x: number) => {},
}: Props) =>
  <div className="star-rating">
    {Array(totalStars).fill(undefined).map((n, i) =>
      <Star key={i}
            selected={i < starsSelected}
            onClick={() => onRate(i + 1)}/>
    )}
    <p>{starsSelected} of {totalStars} stars</p>
  </div>

export default StarRating
