import * as React from 'react';
import './Star.scss'

interface Props {
  selected: boolean,
  onClick: (x: any) => any,
}

const Star = ({
                selected = false,
                onClick = (x: any) => x
}: Props) =>
    <div className={(selected) ? "star selected" : "star"}
         onClick={onClick}>
    </div>

export default Star
