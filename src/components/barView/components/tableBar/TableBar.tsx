import * as React from 'react';
import {IBarMessage} from '../../../interfaces';


const TableBar = ({sid, aid, body}: IBarMessage) => {
    let ts = body.ts;
    let {r, g, b} = body.bar;
    return <table>
      <thead>
        <tr>
          <td>sid</td>
          <td>aid</td>
          <td>ts</td>
          <td>r</td>
          <td>g</td>
          <td>b</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{sid}</td>
          <td>{aid}</td>
          <td>{ts}</td>
          <td>{r}</td>
          <td>{g}</td>
          <td>{b}</td>
        </tr>
      </tbody>
    </table>
  }
export default TableBar;