import * as React from 'react';
import {LineChartPoint} from '../../../interfaces';

interface Props {
  data: Array<LineChartPoint>
}

const LineChartPoints = ({data}: Props) =>
  <table>
    <thead>
    <tr>
      <td>date</td>
      <td>value</td>
    </tr>
    </thead>
    <tbody>
    {data.map((lineChartPoint, key) =>
      <tr
        {...{key}}
      >
        <td>{lineChartPoint.date}</td>
        <td>{lineChartPoint.value}</td>
      </tr>
    )}
    </tbody>
  </table>

export default LineChartPoints;