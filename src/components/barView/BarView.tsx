import * as React from 'react';
import {Color, IBarMessage, IBodyBar, initBarMessage, LineChartPoint} from '../interfaces';
import TableBar from "./components/tableBar/TableBar";
import {ACTION, CrxClient, TYPE} from '../CrxClient';
import {Subscription} from "rxjs";
import LineChartPoints from "./components/lineChartPoints/LineChartPoints";

interface State {
  dataForChart: Array<LineChartPoint>;
  data: Array<IBodyBar>;
  message: IBarMessage;
  isBarViewSubscribe: boolean;
}

interface Props {
  crxClient: CrxClient;
  isBarSubscribeLast?: boolean;
}

export class BarView extends React.Component<Props, State> {
  public static defaultProps: Partial<Props> = {
    isBarSubscribeLast: false,
  }
  private subscription: Subscription;

  constructor(props: any) {
    super(props);
    this.state = {
      dataForChart: [],
      data: [],
      message: initBarMessage,
      isBarViewSubscribe: false
    }
  }

  toggleGettingDataFromBar1 = () => {
    let {
      props: {
        crxClient,
      },
    } = this;

    if (this.state.isBarViewSubscribe) {
      crxClient.unsubscribeBar();
      this.setState({
        isBarViewSubscribe: false,
        message: initBarMessage,
      });
      this.subscription.unsubscribe();
    } else {
      crxClient.subscribeBar();
      this.subscription = crxClient.subject$
        .subscribe(
          (message: any) => {
            console.log('message from Subscribe: ', message)
            if (message.type === TYPE.EVENT && message.action === ACTION.BAR) {
              let data = this.state.data;
              data.push(message.body);
              data.shift();
              this.setState({
                message,
                data,
              });
            } else if (message.type === TYPE.RESPONSE && message.action === ACTION.BAR_HISTORY) {
              let bodyBars = message.body;
              this.setState({
                data: bodyBars,
              });
            }
          }
        );

      this.setState({
        // message: initBarMessage,
        isBarViewSubscribe: true,
      });
    }
  }

  rgbDataToCharData = (rgbData: Array<IBodyBar>, color1: Color = 'r', color2: Color = 'b') => {
    let chartData: Array<LineChartPoint> = [];
    rgbData.forEach(bar => {
      let value = bar.bar[color1] / bar.bar[color2];
      chartData.push({
        date: bar.ts,
        value,
      })
    })
    return chartData;
  }

  public render(): JSX.Element {
    const {
      props: {
        isBarSubscribeLast,
      },
      state: {
        isBarViewSubscribe,
        message,
        data
      },
      toggleGettingDataFromBar1,
      rgbDataToCharData,
    } = this;

    return <div>
      <div>BAR VIEW</div>
      <TableBar {...message}/>
      <button
        onClick={toggleGettingDataFromBar1}
      >{
        isBarViewSubscribe ?
          isBarSubscribeLast ?
            "Unsubscribe" :
            "Stop getting data" :
          "Start gettting data"
      }</button>
      {/* TODO: replace LineChartPoints on LineChart from amcharts */}
      {isBarViewSubscribe && <LineChartPoints data={rgbDataToCharData(data)}/>}
      <br/>
      <br/>
    </div>
  }
}